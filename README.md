DXF and STL files for laser cutting or 3D-printing the following enclosures: Raspberry Shake RS1D, RS3D, RS4D, RBOOM and RS&BOOM. 

Designed by: Amilcar Wachter.

# Supported Raspberry Pis

See: https://manual.raspberryshake.org/specifications.html#rpi-s-supported

# Special note about geophone straps

Please note that the RS1D, RS4D, and RS&BOOM straps and enclosures are designed for a Racotech RGI-20DX geophone and the RS3D strap and enclosures for Sunfull PS-4.5B geophones.

# Special note about infrasound straps for the mechanical filter (tubes)

As these are 3D printed parts, you will only find them in the STL folder for the RBOOM and RS&BOOM. 

# Special note about STL files

Please note that if you plan to 3D print the enclosure, you will likely have to modify the STL files. They were designed for a laser cutter which is more precise than a 3D printer and for clear lids, so the logo and words will have to be moved from the bottom to the top of the lid. 

# Hardware List (screws, standoffs, etc.) for Raspberry Shake RS1D
### RPiModelB base

	x2 Standoff 2.5mm x 25mm – 4.5mm OD aluminium
	x2 Standoff 2.5mm x 13mm – 4.5mm OD aluminium
	x2 Phillip Screw 2.5mm x 8mm stainless steel

### RPiModelB+_2_3_4 base

	x3 Standoff 2.5mm x 25mm x 4.5mm OD aluminium
	x4 Standoff 2.5mm x 13mm x 4.5mm OD aluminium
	x4 Phillip Screw 2.5mm x 8mm stainless steel
	x1 Lock Washer # 2.5mm

### RPiModelZero base

	x2 Standoff 2.5mm x 25mm – 4.5mm OD aluminium 
	x4 Standoff 2.5mm x 13mm – 4.5mm OD aluminium
	x4 Phillip Screw 2.5mm x 8mm stainless steel
	x2 Lock Washer # 2.5mm

### Leveling feet (common to all models)

	x3 Cone Point Screw M5 x 25mm
	x3 Nuts M5
	x1 Hex drive Arm standard ALX2,5mm (Tool)

### geophoneHolder (common to all RPi models)

	x2 Phillip Screw 3mm x 10mm stainless steel
	x2 Lock washer 3mm

# Hardware List (screws, standoffs, etc.) for Raspberry Shake RS3D

### RPiModelB+_2_3_4 base

	x3 Standoff 2.5mm x 20mm x 4.5mm OD aluminium
	x3 Standoff 2.5mm x 11mm x 4.5mm OD aluminium
	x4 Standoff 2.5mm x 13mm x 4.5mm OD aluminium
	x4 Phillip Screw 2.5mm x 8mm stainless steel
	x1 Lock Washer # 2.5mm	

### geophoneHolder

	x6 Phillip Screw 3mm x 10mm stainless steel 
	x6 Lock washer 3mm

# Hardware List (screws, standoffs, etc.) for Raspberry Shake RS4D

### RPiModelB+_2_3_4 base

	x3 Standoff 2.5mm x 13mm x 4.5mm OD aluminium
	x4 Standoff 2.5mm x 11mm x 4.5mm OD aluminium
	x4 Standoff 2.5mm x 13mm x 4.5mm OD aluminium
	x4 Phillip Screw 2.5mm x 8mm stainless steel
	x1 Lock Washer # 2.5mm

### geophoneHolder

	x2 Phillip Screw 3mm x 10mm stainless steel
	x2 Lock washer 3mm

### Mounting screw to fix to ground

	x1 Parker 4.2 x 38 mm
	x1 Conical spring: large end OD 8mm; small end ID 3.4 mm; free length 6.5 mm

# Hardware List (screws, standoffs, etc.) for Raspberry "Shake & Boom" (RS&BOOM)

### RPiModel B+_2_3_4 base

	x4 Standoff 2.5mm x 13mm x 4.5mm OD aluminium
	x7 Standoff 2.5mm x 11mm x 4.5mm OD aluminium
	x3 Standoff 2.5mm x 19mm x 4.5mm OD aluminium
	x4 Phillip Screw 2.5mm x 8mm stainless steel
	x1 Lock Washer # 2.5mm
	x2 M3-0.5 x 18 mm Machine Screws/ Phillips/ Pan Head/ 18-8 Stainless Steel
	x2 M3-0.5 Hex Jam Nuts / 18-8 Stainless Steel
	x4 M3 Split Lock Washers / 18-8 Stainless Steel
	x2 M3-0.5 x 14 mm Machine Screws / Phillips / Pan Head / 18-8 Stainless Steel
	x3 M5-0.8 x 25mm Hex Drive Cone Point Stainless Steel Socket Set Screw
	x3 M5-0.8 Stainless Steel Jam Nut 
	x1 M2.5 Short Arm Hex Keys / Alloy Steel / Black Oxide

### RPiModel Zero base

	x2 Standoff 2.5mm x 13mm x 4.5mm OD aluminium
	x5 Standoff 2.5mm x 11mm x 4.5mm OD aluminium
	x3 Standoff 2.5mm x 19mm x 4.5mm OD aluminium
	x2 Standoff 2.5mm x 25mm x 4.5mm OD aluminium
	x4 Phillip Screw 2.5mm x 8mm stainless steel
	x1 Lock Washer # 2.5mm
	x2 M3-0.5 x 18 mm Machine Screws/ Phillips/ Pan Head/ 18-8 Stainless Steel
	x2 M3-0.5 Hex Jam Nuts / 18-8 Stainless Steel
	x4 M3 Split Lock Washers / 18-8 Stainless Steel
	x2 M3-0.5 x 14 mm Machine Screws / Phillips / Pan Head / 18-8 Stainless Steel
	x3 M5-0.8 x 25mm Hex Drive Cone Point Stainless Steel Socket Set Screw
	x3 M5-0.8 Stainless Steel Jam Nut 
	x1 M2.5 Short Arm Hex Keys / Alloy Steel / Black Oxide

### geophoneHolder

	x2 Phillip Screw 3mm x 10mm stainless steel
	x2 Lock washer 3mm


# Hardware List (screws, standoffs, etc.) for Raspberry Boom (RBOOM)

### RPiModel B+_2_3_4 base

	x4 Standoff 2.5mm x 13mm x 4.5mm OD aluminium
	x7 Standoff 2.5mm x 11mm x 4.5mm OD aluminium
	x3 Standoff 2.5mm x 19mm x 4.5mm OD aluminium
	x4 Phillip Screw 2.5mm x 8mm stainless steel
	x1 Lock Washer # 2.5mm
	x2 M3-0.5 x 18 mm Machine Screws / Phillips / Pan Head / 18-8 Stainless Steel
	x2 M3-0.5 Hex Jam Nuts / 18-8 Stainless Steel
	x4 M3 Split Lock Washers / 18-8 Stainless Steel
	x2 M3-0.5 x 14 mm Machine Screws / Phillips / Pan Head / 18-8 Stainless Steel

### RPiModel Zero base
       
	x2 Standoff 2.5mm x 13mm x 4.5mm OD aluminium
	x5 Standoff 2.5mm x 11mm x 4.5mm OD aluminium
	x3 Standoff 2.5mm x 19mm x 4.5mm OD aluminium
	x2 Standoff 2.5mm x 25mm x 4.5mm OD aluminium
	x4 Phillip Screw 2.5mm x 8mm stainless steel
	x1 Lock Washer # 2.5mm
	x2 M3-0.5 x 18 mm Machine Screws/ Phillips/ Pan Head/ 18-8 Stainless Steel
	x2 M3-0.5 Hex Jam Nuts / 18-8 Stainless Steel
	x4 M3 Split Lock Washers / 18-8 Stainless Steel
 	x2 M3-0.5 x 14 mm Machine Screws / Phillips / Pan Head / 18-8 Stainless Steel

# More information

For more information about the Raspberry Shake Personal Seismograph, visit [http://raspberryshake.org/](http://raspberryshake.org/).
